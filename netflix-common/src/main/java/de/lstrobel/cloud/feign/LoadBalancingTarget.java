package de.lstrobel.cloud.feign;

import com.netflix.loadbalancer.Server;
import feign.Request;
import feign.RequestTemplate;
import feign.RetryableException;
import feign.Target;

import static java.lang.String.format;

public class LoadBalancingTarget<T> implements Target<T> {
    private final Class<T> type;
    private final LoadBalancer loadBalancer;
    private final String schemeName;
    private final String serviceName;

    public LoadBalancingTarget(Class<T> type, LoadBalancer loadBalancer, String schemeName, String serviceName) {
        this.type = type;
        this.loadBalancer = loadBalancer;
        this.schemeName = schemeName;
        this.serviceName = serviceName;
    }

    public Class<T> type() {
        return type;
    }

    public String name() {
        return serviceName;
    }

    public String url() {
        return null;
    }

    public Request apply(RequestTemplate input) {
        Server server = loadBalancer.chooseServer(name());
        String url = format("%s://%s", schemeName, server.getHostPort());
        System.out.println("LST:\ttrying url - " + url);
        input.insert(0, url);
        return input.request();
    }
}
