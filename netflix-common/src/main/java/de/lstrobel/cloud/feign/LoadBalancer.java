package de.lstrobel.cloud.feign;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.DiscoveryClient;
import com.netflix.loadbalancer.AbstractLoadBalancer;
import com.netflix.loadbalancer.LoadBalancerStats;
import com.netflix.loadbalancer.Server;

import java.util.List;

public class LoadBalancer extends AbstractLoadBalancer {

    private boolean useSecureProtocol = false;
    private DiscoveryClient discoveryClient;

    public static LoadBalancer create(boolean useSecureProtocol, DiscoveryClient discoveryClient){
        return new LoadBalancer(useSecureProtocol, discoveryClient);

    }
    private LoadBalancer(boolean useSecureProtocol, DiscoveryClient discoveryClient) {
        this.useSecureProtocol = useSecureProtocol;
        this.discoveryClient = discoveryClient;
    }

    public List<Server> getServerList(boolean availableOnly) {
        throw new RuntimeException("Not implemented!");
    }

    public List<Server> getServerList(ServerGroup serverGroup) {
        throw new RuntimeException("Not implemented!");
    }

    public LoadBalancerStats getLoadBalancerStats() {
        throw new RuntimeException("Not implemented!");
    }

    public void addServers(List<Server> newServers) {
        throw new RuntimeException("Not implemented!");
    }

    public Server chooseServer(Object key) {
        int infoCount = discoveryClient.getInstancesByVipAddress(key.toString(), useSecureProtocol).size();

        while (infoCount-- > 0) {
            InstanceInfo info = discoveryClient.getNextServerFromEureka(key.toString(), useSecureProtocol);
            if (info.getStatus() == InstanceInfo.InstanceStatus.UP) {
                return new MyServer(info);
            }
        }
        throw new RuntimeException("No instances available for service name: " + key.toString());
    }

    public void markServerDown(Server server) {
        if (server instanceof MyServer) {
            MyServer myServer= (MyServer) server;
            myServer.instanceInfo.setStatus(InstanceInfo.InstanceStatus.DOWN);
        } else
            server.setAlive(false);
    }

    static class MyServer extends Server {
        final InstanceInfo instanceInfo;
        MyServer(InstanceInfo instanceInfo) {
            super(instanceInfo.getHostName(), instanceInfo.getPort());
            this.instanceInfo = instanceInfo;
        }
    }

}
