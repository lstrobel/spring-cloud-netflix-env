package de.lstrobel.cloud.feign;

import de.lstrobel.cloud.MetaDataHolder;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class FeignRequestInterceptor implements RequestInterceptor {

    @Autowired
    MetaDataHolder metaDataHolder;

    @Override
    public void apply(RequestTemplate template) {
        Map<String, String> metaData = metaDataHolder.getMetaData();

        for(Map.Entry<String, String> entry : metaData.entrySet()){
            template.header(entry.getKey(), entry.getValue());
        }
    }
}
