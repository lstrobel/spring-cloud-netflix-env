package de.lstrobel.cloud.feign;

import com.netflix.discovery.DiscoveryClient;
import de.lstrobel.cloud.ServiceFactory;
import feign.RequestInterceptor;
import feign.Retryer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.feign.FeignConfiguration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class FeignServiceFactory extends FeignConfiguration implements ServiceFactory {

    @Value(value = "${eureka.feign.useSecureProtocol:false}")
    boolean useSecureProtocol;

    @Value(value = "${eureka.feign.retry.interval.ms:100}")
    int intervalTimeMs;
    @Value(value = "${eureka.feign.retry.maxinterval.ms:1000}")
    int maxIntervalTimeMs;
    @Value(value = "${eureka.feign.retry.maxattempts:1}")
    int maxAttempts;
    @Autowired
    @Lazy
    DiscoveryClient discoveryClient;

    @Autowired
    RequestInterceptor requestInterceptor;


    @Override
    public <T> T createService(Class<T> type, String serviceName) {

        return this.feign()
                .retryer(new Retryer.Default(intervalTimeMs, maxIntervalTimeMs, maxAttempts))
                .requestInterceptor(requestInterceptor)
                .target(new LoadBalancingTarget<>(type,
                        LoadBalancer.create(false, discoveryClient),
                        useSecureProtocol ? "https" : "http",
                        serviceName));
    }
}
