package de.lstrobel.cloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import law.smart.library.checkout.CheckoutService;
import law.smart.library.checkout.dto.ReceiptDTO;
import org.springframework.stereotype.Component;

@Component
public class CheckoutServiceImpl implements CheckoutService {

    @HystrixCommand(fallbackMethod = "defaultReceipt",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "250")
            })
    public ReceiptDTO checkout(String id) {
        ReceiptDTO result = new ReceiptDTO();
        result.setBookId(id);
        //result.setReturnDate(LocalDate.now());
        try {
            Thread.sleep(  (long)Math.random() * 500);
        } catch (InterruptedException ignore) {
        }
        System.out.println("LST:\tcheckout book: " + id);
        return result;
    }

    public ReceiptDTO defaultReceipt(String id) {
        System.out.println("LST:\tdefaultReceipt: " + id);
        ReceiptDTO result = new ReceiptDTO();
        result.setBookId("defaultFor: " + id);
        return result;
    }

}
