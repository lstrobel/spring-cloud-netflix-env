package de.lstrobel.cloud.configuration;

import org.springframework.beans.factory.config.YamlMapFactoryBean;
import org.springframework.boot.bind.YamlJavaBeanPropertyConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class ServiceConfig {
    private List<ServiceConfigEntry> services = new ArrayList<>();

    public ServiceConfig() {
        services.add(new ServiceConfigEntry("eureka-server", "1.0-SNAPSHOT", 8761, 8762));
        services.add(new ServiceConfigEntry("eureka-server", "1.0-SNAPSHOT", 8762, 8761));
        services.add(new ServiceConfigEntry("book-service", "1.0-SNAPSHOT", 8091, 8761));
        services.add(new ServiceConfigEntry("book-service", "1.0-SNAPSHOT", 8092, 8762));
        services.add(new ServiceConfigEntry("book-checkout-service", "1.0-SNAPSHOT", 8093, 8761));
        services.add(new ServiceConfigEntry("book-checkout-service", "1.0-SNAPSHOT", 8094, 8762));
        services.add(new ServiceConfigEntry("book-search-service", "1.0-SNAPSHOT", 8095, 8761));
        services.add(new ServiceConfigEntry("book-search-service", "1.0-SNAPSHOT", 8096, 8762));
        services.add(new ServiceConfigEntry("hystrix-dashboard", "1.0-SNAPSHOT", 8097, 8761));
    }

    @Bean(name = "services")
    public List<ServiceConfigEntry> getServices() {
        return services;
    }

    public void setServices(List<ServiceConfigEntry> services) {
        this.services = services;
    }
}
