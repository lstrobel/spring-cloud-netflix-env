package de.lstrobel.cloud.configuration;

public class ServiceConfigEntry {
    private int port;
    private int eport;
    private String name;
    private String version;

    public ServiceConfigEntry(String name, String version, int port, int eport) {
        this.port = port;
        this.eport = eport;
        this.name = name;
        this.version = version;
    }

    public String toString() {
        return "ServiceConfigEntry{" +
                "port=" + port +
                ", eport=" + eport +
                ", name='" + name + '\'' +
                ", version='" + version + '\'' +
                '}';
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getEport() {
        return eport;
    }

    public void setEport(int eport) {
        this.eport = eport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
