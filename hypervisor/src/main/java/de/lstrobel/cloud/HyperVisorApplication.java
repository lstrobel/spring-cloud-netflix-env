/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.lstrobel.cloud;

import com.netflix.discovery.DiscoveryClient;
import de.lstrobel.cloud.configuration.ServiceConfig;
import de.lstrobel.cloud.configuration.ServiceConfigEntry;
import de.lstrobel.cloud.feign.LoadBalancer;
import de.lstrobel.cloud.feign.LoadBalancingTarget;
import law.smart.EurekatestService;
import law.smart.library.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.FeignConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@EnableAutoConfiguration
@Import(ServiceConfig.class)
@ComponentScan
@EnableDiscoveryClient
@RestController
public class HyperVisorApplication extends FeignConfiguration {
    private List<Process> processes = new ArrayList<>();

    @Resource
    private List<ServiceConfigEntry> services;

    @Autowired(required = false)
    @Lazy
    SearchService searchService;

    @Autowired(required = false)
    @Lazy
    EurekatestService eurekatestService;

    @Autowired
    DiscoveryClient discoveryClient;
    private boolean isStressing = false;

    @RequestMapping(method = RequestMethod.GET, value = "/startUp")
    public Status handleStartUp() throws IOException {
        processes = services.stream().map(this::createProcess).collect(Collectors.toList());
        return new Status().setStatus("Up!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/startEurekaTest")
    public Status handleEurekaTest() throws IOException {
        isStressing = true;
        while (isStressing) {
            System.out.println(eurekatestService.greeting("Ballo"));
        }

        return new Status().setStatus("beeing stressed");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/shutDown")
    public Status handleShutDown() {
        return new Status().setStatus("Down!");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stressStart")
    public Status handleStressStart() {
        isStressing = true;
        while (isStressing) {
            System.out.println(searchService.findByAuthor("Ballo"));
        }

        return new Status().setStatus("beeing stressed");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/stressStop")
    public Status handleStressStop() {
        isStressing = false;
        return new Status().setStatus("beeing relaxed");
    }


    Process createProcess(ServiceConfigEntry configEntry) {
        try {
            return new ProcessBuilder().command(
                    "java",
                    "-Dport=" + configEntry.getPort(),
                    "-Deport=" + configEntry.getEport(),
                    "-jar",
                    MessageFormat.format("/dev/prjRoot/java8/spring-cloud-netflix-env/{0}/target/{0}-{1}.jar", configEntry.getName(), configEntry.getVersion())
            )
                    .redirectOutput(new File(MessageFormat.format("/var/spring-cloud/log/{0}-{1}-redirekt.log", configEntry.getName(), configEntry.getPort())))
                    .redirectError(new File(MessageFormat.format("/var/spring-cloud/log/{0}-{1}-redirekt-error.log", configEntry.getName(), configEntry.getPort())))
                    .directory(new File("/var/spring-cloud"))
                    .start();
        } catch (IOException e) {
            throw new RuntimeException(("cannot create process for serviceConfig: " + configEntry), e);
        }
    }

    @Bean
    @Lazy
    public SearchService searchService() {
        boolean useSecureProtocol = false;
        return this.feign().target(new LoadBalancingTarget<>(
                SearchService.class,
                LoadBalancer.create(false, discoveryClient),
                useSecureProtocol ? "https" : "http",
                "book-search-service"));
    }

    @Bean
    @Lazy
    public EurekatestService eurekatestService() {
        boolean useSecureProtocol = false;
        return this.feign().target(new LoadBalancingTarget<>(
                EurekatestService.class,
                LoadBalancer.create(false, discoveryClient),
                useSecureProtocol ? "https" : "http",
                "eurekatest"));
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(HyperVisorApplication.class, args);
    }

    static class Status {
        private String status;

        public Status setStatus(String status) {
            this.status = status;
            return this;
        }
    }

}
