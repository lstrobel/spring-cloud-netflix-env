package de.lstrobel.cloud;

import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class MetaDataHolder {

    final ThreadLocal<Map<String,String>> metaData = new ThreadLocal<>();

    public Map<String, String> getMetaData(){
        if (metaData.get() == null)
            return Collections.unmodifiableMap(new HashMap<>());
        return metaData.get();
    }

    public void setMetaData(Map<String,String> metaDataMap){
        if (metaDataMap == null) {
            this.metaData.remove();
            return;
        }
        this.metaData.set(metaDataMap);
    }
}
