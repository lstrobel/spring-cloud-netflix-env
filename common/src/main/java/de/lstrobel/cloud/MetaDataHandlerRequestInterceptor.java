package de.lstrobel.cloud;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.handler.WebRequestHandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class MetaDataHandlerRequestInterceptor extends HandlerInterceptorAdapter {

    @Value(value = "${header.to.be.set:noheader")
    String header;

    @Autowired
    MetaDataHolder meatDataHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Map<String, String> headers = new HashMap<>();
        if (!header.equalsIgnoreCase("noheader")){
            headers.put(header, request.getHeader(header));
            meatDataHolder.setMetaData(Collections.unmodifiableMap(headers));
        }
        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        meatDataHolder.setMetaData(null);
        super.postHandle(request, response, handler, modelAndView);
    }
}
