package de.lstrobel.cloud;

public interface ServiceFactory {
    <T> T createService(Class<T> type, String serviceName);
}
