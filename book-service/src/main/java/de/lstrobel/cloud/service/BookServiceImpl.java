package de.lstrobel.cloud.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import law.smart.library.books.BookService;
import law.smart.library.books.dto.BookDTO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookServiceImpl implements BookService{
    @HystrixCommand(fallbackMethod = "defaultBookList",
            commandProperties = {
                    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "250")
            })
    public List<BookDTO> getAllBooks() {
        try {
            Thread.sleep( (long)Math.random() * 500);
        } catch (InterruptedException ignore) {
        }
        List<BookDTO> result = createBookList();
        System.out.println("LST:\tgetAllBooks() ships: " + result);
        return result;
    }

    public List<BookDTO> defaultBookList() {
        System.out.println("LST:\tdefaultBookList()");
        return new ArrayList<>();
    }

    private List<BookDTO> createBookList(){
        List<BookDTO> result = new ArrayList<BookDTO>();
        BookDTO book;
        for (int ii=0;ii<10;ii++){
            book=new BookDTO();
            book.setId(Integer.toString(ii));
            if (ii % 2 == 0) {
                book.setAuthor("Hallo");
            } else{
                book.setAuthor("Ballo");
            }
            book.setTitle("Titel-" + ii);
            result.add(book);
        }
        return result;
    }
}
