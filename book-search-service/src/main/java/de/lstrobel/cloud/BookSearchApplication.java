/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.lstrobel.cloud;

import com.netflix.discovery.DiscoveryClient;
import de.lstrobel.cloud.feign.LoadBalancer;
import de.lstrobel.cloud.feign.LoadBalancingTarget;
import feign.RetryableException;
import law.smart.library.books.BookService;
import law.smart.library.books.dto.BookDTO;
import law.smart.library.checkout.CheckoutService;
import law.smart.library.checkout.dto.ReceiptDTO;
import law.smart.library.search.SearchService;
import law.smart.library.search.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.FeignConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
@EnableAutoConfiguration
@EnableDiscoveryClient
//@EnableCircuitBreaker
@ComponentScan
@RestController
public class BookSearchApplication extends FeignConfiguration implements SearchService {

    @Autowired(required = false)
    @Lazy
    BookService bookService;

    @Autowired(required = false)
    @Lazy
    CheckoutService checkoutService;

    @Autowired
    DiscoveryClient discoveryClient;


    @RequestMapping(value = "/bookSearch/{author}")
    public List<SearchResultDTO> findByAuthor(@PathVariable(value = "author") String author) {
        List<SearchResultDTO> result = new ArrayList<SearchResultDTO>();
        System.out.println("LST:\tfindByAuthor()");
        List<BookDTO> books = books();
        System.out.println("LST:\tgot books: " + books);
        result.addAll(
                books.stream()
                        .filter((b) -> author.equalsIgnoreCase(b.getAuthor()))
                        .map(BOOK2SEARCHRESULT)
                        .collect(Collectors.toList())
        );
        result.forEach((sr) -> {
            ReceiptDTO r = receiptDTO(sr.getId());
            sr.setReturnDate(LocalDate.now());
            sr.setStatus("Weiss nicht was das ist!");
        });
        return result;
    }

    ReceiptDTO receiptDTO(String id) {
        while (true) {
            try {
                return checkoutService.checkout(id);
            } catch (RetryableException re) {
                System.out.println(re.getMessage());
            }
        }
    }

    List<BookDTO> books() {
        while (true) {
            try {
                return bookService.getAllBooks();
            } catch (RetryableException re) {
                System.out.println(re.getMessage());
            }
        }
    }

    @Bean
    @Lazy
    public BookService bookService() {
        boolean useSecureProtocol = false;
        return this.feign().target(new LoadBalancingTarget<>(
                BookService.class,
                LoadBalancer.create(false, discoveryClient),
                useSecureProtocol ? "https" : "http",
                "book-service"));
    }

    @Bean
    @Lazy
    public CheckoutService checkoutService() {
        boolean useSecureProtocol = false;
        return this.feign().target(new LoadBalancingTarget<>(
                CheckoutService.class,
                LoadBalancer.create(useSecureProtocol, discoveryClient),
                useSecureProtocol ? "https" : "http",
                "book-checkout-service"));
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BookSearchApplication.class, args);
        System.out.println("LST:\tbook-search-service is running");
    }

    static Function<BookDTO, SearchResultDTO> BOOK2SEARCHRESULT = new Function<BookDTO, SearchResultDTO>() {
        public SearchResultDTO apply(BookDTO bookDTO) {
            SearchResultDTO result = new SearchResultDTO();
            result.setAuthor(bookDTO.getAuthor());
            result.setId(bookDTO.getId());
            result.setTitle(bookDTO.getTitle());
            return result;
        }
    };
}
